import amqplib, { Channel } from "amqplib";
import { EXCHANGE_NAME, NOTIFICATION_SERVICE, QUEUE_NAME } from "../../config/notification.constant";



export const createChannel = async () => {
    try {
        const url: any = process.env.MSG_QUEUE_URL
        const connection = await amqplib.connect(url);
        const channel = await connection.createChannel();
        await channel.assertQueue(EXCHANGE_NAME, { durable: false });
        return channel;
    } catch (err) {
        throw err;
    }
};

export const publishMessage = (channel: Channel, service: any, msg: any): void => {
    channel.publish(EXCHANGE_NAME, service, Buffer.from(msg));
    console.log("Sent: ", msg);
};

export const subscribeMessage = async (channel: Channel, service: any): Promise<void> => {
    // channel.assertQueue(EXCHANGE_NAME, {durable: false})

    // const q = await channel.assertQueue("", { exclusive: true });
    // await channel.assertExchange(EXCHANGE_NAME, 'direct', { durable: false });
    // await channel.bindQueue(q.queue, EXCHANGE_NAME, NOTIFICATION_SERVICE);

    // channel.consume(
    //     q.queue,
    //     (msg: any) => {
    //         const msgEvent = JSON.parse(msg.content);
    //         console.log("the message is:", msg.content.toString());
    //         if (msg && msg.content) {
    //             service.subscribeEvents(msgEvent);
    //         }
    //         console.log("RECEIVED [X]");
    //     },
    //     {
    //         noAck: true,
    //     }
    // );

    await channel.assertQueue(QUEUE_NAME, { durable: false });
    await channel.consume(
        QUEUE_NAME,
        (msg: any) => {
            const msgEvent = JSON.parse(msg.content);
            console.log("the message is:", msg.content.toString());
            if (msg && msg.content) {
                service.subscribeEvents(msgEvent);
            }
            console.log("RECEIVED [X]");
        },
        {
            noAck: true,
        }
    );
};