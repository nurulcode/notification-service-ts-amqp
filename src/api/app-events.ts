import { Request, Response, Express } from "express";
import NotificationService from "../services/notification.service";

const appEvens = (app: Express) => {
    const service = new NotificationService();
    app.use('/app-events', async (req, res) => {

        const { payload } = req.body;

        service.subscribeEvents(payload);

        console.log("============= Notification ================");
        console.log(payload);
        res.json(payload);
    });
}

