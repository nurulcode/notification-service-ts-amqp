// src/routes.ts

import express from 'express';
import NotificationController from '../controllers/notification.controller';

const router = express.Router();
const inst = new NotificationController()
router.get('/', inst.getNotifications);
router.get('/:id', inst.getNotification);

export default router
