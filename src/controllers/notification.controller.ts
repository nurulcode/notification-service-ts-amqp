import { Request, Response } from 'express';
import NotificationService from '../services/notification.service';

class NotificationController {
    private notificationService: NotificationService;
    constructor() {
        this.notificationService = new NotificationService();
    }

    getNotifications = async (req: Request, res: Response): Promise<void> => {
        try {
            const results = await this.notificationService.getNotifications();
            res.status(200).json({
                status: "success",
                data: results
            });
        } catch (error: any) {
            res.status(500).json({
                status: "error",
                message: error.message || "Something went wrong",
            });
        }
    }

    getNotification = async (req: Request, res: Response): Promise<void> => {
        try {
            const id = parseInt(req.params.id);
            const results = await this.notificationService.getNotificationUser(id);
            res.status(200).json({
                status: "success",
                data: results
            });
        } catch (error: any) {
            res.status(500).json({
                status: "error",
                message: error.message || "Something went wrong",
            });
        }
    }
}

export default NotificationController;