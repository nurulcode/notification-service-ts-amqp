import { PrismaClient } from '@prisma/client';

class NotificationRepository {
    private prisma: PrismaClient

    constructor() {
        this.prisma = new PrismaClient()
    }
    async getAll(): Promise<any> {
        return await this.prisma.notification.findMany()
    }

    async get({ ...data }): Promise<any> {
        return await this.prisma.notification.findMany(
            {
                where: data
            }
        )
    }
    async create(payload: any): Promise<any> {
        return await this.prisma.notification.create({
            data: payload,
        })

    }
}

export default NotificationRepository