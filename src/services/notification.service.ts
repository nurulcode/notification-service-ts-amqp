import { INotification } from "../interfaces/notifications.interfaces"
import NotificationRepository from "../repository/notification.repository"

class NotificationService {
    private notificationRepository: NotificationRepository
    constructor() {
        this.notificationRepository = new NotificationRepository()
    }

    async getNotifications(): Promise<INotification[]> {
        try {
            const results = await this.notificationRepository.getAll();
            return results;
        } catch (err) {
            console.log(err);
            throw new Error("Data Not found!");
        }
    }

    async getNotificationUser(user_id: number): Promise<INotification> {
        try {
            const results = await this.notificationRepository.get({
                user_id: user_id
            });
            return results;
        } catch (err) {
            console.log(err);
            throw new Error("Data Not found!");
        }
    }
    async getNotification(id: number): Promise<INotification> {
        try {
            const results = await this.notificationRepository.get({
                id: id
            });
            return results;
        } catch (err) {
            console.log(err);
            throw new Error("Data Not found!");
        }
    }
    async createNotification(payload: any): Promise<any> {
        try {
            const results = await this.notificationRepository.create({
                user_id: parseInt(payload.user_id),
                status: 'success',
                metadata: {
                    transaction_time: payload.transaction_time,
                    gross_amount: payload.gross_amount
                }
            });
            return results;
        } catch (err) {
            console.log(err);
            throw new Error("Insert data failed!");
        }
    }

    async subscribeEvents(payload: any) {
        console.log('Notification.... Events')
        const { event, data } = payload;
        switch (event) {
            case 'CREATE_NOTIFICATION':
                await this.createNotification(data)
                break;
            default:
                console.log(`Unhandled event: ${event}`);
                break;
        }
    }


}

export default NotificationService