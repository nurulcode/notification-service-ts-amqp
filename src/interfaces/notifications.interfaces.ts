export interface INotification extends Record<string, any> {
    id: number,
    user_id: number,
    status: string,
    metadata: Record<string, any>,
    created_at: Date,
    updated_at: Date,
}