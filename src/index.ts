import express from "express";
import dotenv from "dotenv"
import notificationRoute from "./routes/notification.route";
import { createChannel, subscribeMessage } from "./api/utils/events";
import NotificationService from "./services/notification.service";

dotenv.config()
const app = express();

app.use(express.json());

app.use('/api/notifications', notificationRoute)


const appEven = async (service : any) => {
    const channel = await createChannel()
    subscribeMessage(channel, service);
}

appEven(new NotificationService())

const port = process.env.PORT || 3004
app.listen(port, () => {
    console.log('listening on port ' + port);
});